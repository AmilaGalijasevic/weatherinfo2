var departure, destination,
    departureLat, departureLng;


$("#my-form").on("submit", function (event) {
    event.preventDefault();
    var departure = $("#departure").val();
    var destination = $("#destination").val();
    document.getElementById("my-form").reset();

    $.getJSON("https://maps.googleapis.com/maps/api/geocode/json?address=" + departure + "&key=AIzaSyBAdK6_AsKrGFNLDyUfDqYL4MF53CI68rw", function (data, status) {
      
        if (data.status === "ZERO_RESULTS") {
            ($(".errorMessage").html("<div class='errMsg'><h1>Cannot find the requested information</h1><p>(Please check if you entered the correct name and try again) </p></div>"));
            $(".output").hide();

        } else {
            var rawJson = JSON.stringify(data);
            var data = JSON.parse(rawJson);

            var departLocation = {
                lat: data.results[0].geometry.location.lat,
                lng: data.results[0].geometry.location.lng
            }
            getDepartureWeather(departLocation);
        }
    });

    $.getJSON("https://maps.googleapis.com/maps/api/geocode/json?address=" + destination + "&key=AIzaSyBAdK6_AsKrGFNLDyUfDqYL4MF53CI68rw", function (data, status) {
        if (data.status === "ZERO_RESULTS") {
            ($(".errorMessage").html("<div class='errMsg'><h1>Cannot find the requested information</h1><p>(Please check if you entered the correct name and try again) </p></div>"));
            $(".output").hide();
        } else {
            var rawJson = JSON.stringify(data);
            var data = JSON.parse(rawJson);
        
            var destLoacation = {
                lat: data.results[0].geometry.location.lat,
                lng: data.results[0].geometry.location.lng
            }
            getDestinationWeather(destLoacation);
        }
    });

});



$("#my-form2").on("submit", function (event) {
    event.preventDefault();
    var departureLat = $("#departureLat").val();
    var departureLng = $("#departureLng").val();
    var destinationLng = $("#destinationLng").val();
    var destinationLat = $("#destinationLat").val();

    var departLocation = {
        lat: departureLat,
        lng: departureLng
    }

    document.getElementById("my-form2").reset();

    getDepartureWeather(departLocation);

    var destLocation = {
        lat: destinationLat,
        lng: destinationLng
    }
    getDestinationWeather(destLocation);

});



function getDepartureWeather(data) {


    var lat = data.lat;
    var lng = data.lng;

    if (lat != "") {
        $(".output").show();
        $(".errMsg").hide();
    }

    $.get("https://api.met.no/weatherapi/locationforecast/1.9/?lat=" + lat + "&lon=" + lng, function (result, status) {

        var jsonText = JSON.stringify(xmlToJson(result));
        var json = JSON.parse(jsonText);
       
        if (status === "success") {
            $(".departureTemperature").html(json.weatherdata.product.time[0].location.temperature["@attributes"].value);
            $(".departureDewPoint").html(json.weatherdata.product.time[0].location.dewpointTemperature["@attributes"].value);
            $(".departureHumidity").html(json.weatherdata.product.time[0].location.humidity["@attributes"].value);
            $(".departureWindSpeed").html(json.weatherdata.product.time[0].location.windSpeed["@attributes"].mps);


            $(".departureFog").html(json.weatherdata.product.time[0].location.fog["@attributes"].percent);
            $(".departureLowClouds").html(json.weatherdata.product.time[0].location.lowClouds["@attributes"].percent);
            $(".departureMediumClouds").html(json.weatherdata.product.time[0].location.mediumClouds["@attributes"].percent);
            $(".departureHighClouds").html(json.weatherdata.product.time[0].location.highClouds["@attributes"].percent);

            showDepartFog(json.weatherdata.product.time[0].location.fog["@attributes"].percent);

            showDepartLowClouds(json.weatherdata.product.time[0].location.lowClouds["@attributes"].percent);

            showDepartMediumClouds(json.weatherdata.product.time[0].location.mediumClouds["@attributes"].percent);

            showDepartHighClouds(json.weatherdata.product.time[0].location.highClouds["@attributes"].percent);

        }
    }).fail(function () {
        ($(".errorMessage").html("<div class='errMsg'><h1>Cannot find the requested information</h1><p>(Please check if you entered the correct name and try again) </p></div>"));
        $(".output").hide();
    });


};



function getDestinationWeather(data) {

    var lat = data.lat;
    var lng = data.lng;


    $.get("https://api.met.no/weatherapi/locationforecast/1.9/?lat=" + lat + "&lon=" + lng, function (result, status) {
        var jsonText = JSON.stringify(xmlToJson(result));
        var json = JSON.parse(jsonText);

        $(".destinationTemperature").html(json.weatherdata.product.time[0].location.temperature["@attributes"].value);
        $(".destinationDewPoint").html(json.weatherdata.product.time[0].location.dewpointTemperature["@attributes"].value);
        $(".destinationHumidity").html(json.weatherdata.product.time[0].location.humidity["@attributes"].value);
        $(".destinationWindSpeed").html(json.weatherdata.product.time[0].location.windSpeed["@attributes"].mps);

        $(".destinationFog").html(json.weatherdata.product.time[0].location.fog["@attributes"].percent);
        $(".destinationLowClouds").html(json.weatherdata.product.time[0].location.lowClouds["@attributes"].percent);
        $(".destinationMediumClouds").html(json.weatherdata.product.time[0].location.mediumClouds["@attributes"].percent);
        $(".destinationHighClouds").html(json.weatherdata.product.time[0].location.highClouds["@attributes"].percent);

        showDestFog(json.weatherdata.product.time[0].location.fog["@attributes"].percent);

        showDestLowClouds(json.weatherdata.product.time[0].location.lowClouds["@attributes"].percent);

        showDestMediumClouds(json.weatherdata.product.time[0].location.mediumClouds["@attributes"].percent);

        showDestHighClouds(json.weatherdata.product.time[0].location.highClouds["@attributes"].percent);

    }).fail(function () {
        ($(".errorMessage").html("<div class='errMsg'><h1>Cannot find the requested information</h1><p>(Please check if you entered the correct name and try again) </p></div>"));
        $(".output").hide();
    });


};


function xmlToJson(xml) {

    var obj = {};

    if (xml.nodeType == 1) { // element
        // do attributes
        if (xml.attributes.length > 0) {
            obj["@attributes"] = {};
            for (var j = 0; j < xml.attributes.length; j++) {
                var attribute = xml.attributes.item(j);
                obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
            }
        }
    } else if (xml.nodeType == 3) { // text
        obj = xml.nodeValue;
    }


    if (xml.hasChildNodes()) {
        for (var i = 0; i < xml.childNodes.length; i++) {
            var item = xml.childNodes.item(i);
            var nodeName = item.nodeName;
            if (typeof (obj[nodeName]) == "undefined") {
                obj[nodeName] = xmlToJson(item);
            } else {
                if (typeof (obj[nodeName].push) == "undefined") {
                    var old = obj[nodeName];
                    obj[nodeName] = [];
                    obj[nodeName].push(old);
                }
                obj[nodeName].push(xmlToJson(item));
            }
        }
    }
    return obj;
};

function showDestFog(info) {
    if (info > 20) {
        ($(".destinationFogIcon").html("<span> <i class='fa fa-align-justify' aria-hidden='true'></i><i class='fa fa-align-justify' aria-hidden='true'></i> </span>"));
    } else {
        ($(".destinationFogIcon").html("<span> <i class='fa fa fa-sun-o' aria-hidden='true'></i> </span>"));

    }
}

function showDestLowClouds(info) {
    if (info > 50) {
        ($(".destinationLowCloudsIcon").html("<div class='lowClouds'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div> <div><span> <i class='fa fa-cloud' aria-hidden='true'> </div>"));

    } else if (info < 20) {
        ($(".destinationLowCloudsIcon").html("<div class='lowClouds'> </div>"));

    } else {

        ($(".destinationLowCloudsIcon").html("<div class=' lowClouds cloudsTransparent'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div>"));
    }

}

function showDestMediumClouds(info) {
    if (info > 50) {
        ($(".destinationMediumCloudsIcon").html("<div class='mediumClouds'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div> <div><span> <i class='fa fa-cloud' aria-hidden='true'> </div>"));

    } else if (info < 20) {
        ($(".destinationMediumCloudsIcon").html("<div class='mediumClouds'> </div>"));

    } else {

        ($(".destinationMediumCloudsIcon").html("<div class='mediumClouds cloudsTransparent'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div>"));
    }

}

function showDestHighClouds(info) {
    if (info > 50) {
        ($(".destinationHighCloudsIcon").html("<div class='highClouds'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div> <div><span> <i class='fa fa-cloud' aria-hidden='true'> </div>"));

    } else if (info < 20) {
        ($(".destinationHighCloudsIcon").html("<div class='highClouds'></div>"));

    } else {

        ($(".destinationHighCloudsIcon").html("<div class='highlouds cloudsTransparent'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div>"));
    }

}


function showDepartFog(info) {
    if (info > 20) {
        ($(".departureFogIcon").html("<span> <i class='fa fa-align-justify' aria-hidden='true'></i><i class='fa fa-align-justify' aria-hidden='true'></i> </span>"));
    } else {
        ($(".departureFogIcon").html("<span> <i class='fa fa fa-sun-o' aria-hidden='true'></i> </span>"));

    }
}

function showDepartLowClouds(info) {
    if (info > 50) {
        ($(".departureLowCloudsIcon").html("<div class='lowClouds'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div> <div><span> <i class='fa fa-cloud' aria-hidden='true'> </div>"));

    } else if (info < 20) {
        ($(".departureLowCloudsIcon").html("<div class='lowClouds'><span>  </div>"));

    } else {

        ($(".departureLowCloudsIcon").html("<div class='lowClouds cloudsTransparent'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div>"));
    }

}

function showDepartMediumClouds(info) {
    if (info > 50) {
        ($(".ddepartureMediumCloudsIcon").html("<div class='mediumClouds'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div> <div><span> <i class='fa fa-cloud' aria-hidden='true'> </div>"));

    } else if (info < 20) {
        ($(".departureMediumCloudsIcon").html("<div class='mediumClouds'><span>  </div>"));

    } else {

        ($(".departureMediumCloudsIcon").html("<div class='mediumClouds cloudsTransparent'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div>"));
    }

}

function showDepartHighClouds(info) {
    if (info > 50) {
        ($(".departureHighCloudsIcon").html("<div class='highClouds'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div> <div><span> <i class='fa fa-cloud' aria-hidden='true'> </div>"));

    } else if (info < 20) {
        ($(".departureHighCloudsIcon").html("<div class='highClouds'></div>"));

    } else {

        ($(".departureHighCloudsIcon").html("<div class='highClouds cloudsTransparent'><span> <i class='fa fa-cloud' aria-hidden='true'></i><i class='fa fa-cloud' aria-hidden='true'></i></span> </div>"));
    }

}
